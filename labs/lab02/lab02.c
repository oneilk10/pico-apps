//#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include "pico/stdlib.h"
#include "pico/float.h"

#define PRECISION 100000;        ///< default number of iterations
#define PI        3.14159265359; ///< basis for comparison

/**
 * function that implements the Wallis product algorithm
 * using single-precision floating-point representation
 * 
 * @param precision max number of iterations
 * @return calculated value for PI.
 */

float wallis_float(int precision){
    float result=1;
    float x=1;
    for(int n=1; n<=100000; n++){
        x=2*(float)n;
        result*=(x/(x-1))*(x/(x+1));
    }
    return result*2;
}

/**
 * function that implements the Wallis product algorithm
 * using double-precision floating-point representation
 * 
 * @param precision max number of iterations
 * @return calculated value for PI.
 */

double wallis_double(int precision){
    double result=1;
    double x=1;
    for(int n=1; n<=100000; n++){
        x=2*(double)n;
        result*=(x/(x-1))*(x/(x+1));
    }
    return result*2;
}

/**
 * Main entry point of the program.
*/
int main() {

#ifndef WOKWI
    /// Initialise the IO as we will be using the UART
    /// Only required for hardware and not needed for Wokwi
    stdio_init_all();
#endif

    float pi_float=wallis_float(100000);
    float error_float=pi_float-3.14159265359;

    if(error_float<0)
        error_float=-error_float;

    double pi_double=wallis_double(100000);
    double error_double=pi_double-3.14159265359;

    if(error_double<0)
        error_double=-error_double;
    
    printf("Calculated value for PI (using single-precision): %f\nApproximation error for the single-precision representation: %f\n",pi_float,error_float);
    printf("Calculated value for PI (using double-precision): %f\nApproximation error for the double-precision representation: %f",pi_double,error_double); 

    /// Returning zero indicates everything went okay.
    return 0;
}
