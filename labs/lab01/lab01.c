/// @file lab01.c
#include "pico/stdlib.h"


/**
 * blinks the LED on and off
 * @param led_pin pin number of the LED
 * @param led_delay sleep delay
 * 
 * @return 0
 */
int blink(uint led_pin, uint led_delay) {
  /// Toggle the LED on and then sleep for delay period
  gpio_put(led_pin, 1);
  sleep_ms(led_delay);
  /// Toggle the LED off and then sleep for delay period
  gpio_put(led_pin, 0);
  sleep_ms(led_delay);
  return 0;
}

/**
 * Main entry point of the program.
*/
int main() {

    /// Specify the PIN number and sleep delay
    const uint LED_PIN   =  25;
    const uint LED_DELAY = 500;

    /// Setup the LED pin as an output.
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    /// Do forever...
    while (true) {
      blink(LED_PIN, LED_DELAY);
    }

    /// Should never get here due to infinite while-loop.
    return 0;

}