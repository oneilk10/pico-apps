#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.
#include "pico/time.h"      // required for using timestamps

#define PI        3.14159265359 ///< basis for comparison

/**
 * function that implements the Wallis product algorithm
 * using single-precision floating-point representation
 * 
 * @param precision max number of iterations
 * @return calculated value for PI.
 */

float wallis_float(int precision){
    float result=1;
    float x=1;
    for(int n=1; n<=100000; n++){
        x=2*(float)n;
        result*=(x/(x-1))*(x/(x+1));
    }
    return result*2;
}

/**
 * function that implements the Wallis product algorithm
 * using double-precision floating-point representation
 * 
 * @param precision max number of iterations
 * @return calculated value for PI.
 */

double wallis_double(int precision){
    double result=1;
    double x=1;
    for(int n=1; n<=100000; n++){
        x=2*(double)n;
        result*=(x/(x-1))*(x/(x+1));
    }
    return result*2;
}

/**
 * 
 * @brief This function acts as the main entry-point for core #1.
 *        A function pointer is passed in via the FIFO with one
 *        incoming int32_t used as a parameter. The function will
 *        provide an int32_t return value by pushing it back on 
 *        the FIFO, which also indicates that the result is ready.
 */
void core1_entry() {
    while (1) {
        // Function pointer is passed to us via the FIFO
        // We have one incoming int32_t as a parameter, and will provide an
        // int32_t return value by simply pushing it back on the FIFO
        // which also indicates the result is ready.
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

// Main code entry point for core0.
int main() {

    const int    ITER_MAX   = 100000;
    absolute_time_t start_time, double_start_time, end_time, float_end_time;
    uint64_t sequential_time, parallel_time, float_time, double_time;


    stdio_init_all();
    multicore_launch_core1(core1_entry);

    /// Code for sequential run goes here…
    ///    Take snapshot of timer and store
    start_time = get_absolute_time();
    ///    Run the single-precision Wallis approximation
    wallis_float(ITER_MAX);
    //     get the individual runtime
    float_end_time = get_absolute_time();
    float_time = absolute_time_diff_us(start_time, float_end_time);
    ///    Run the double-precision Wallis approximation
    wallis_double(ITER_MAX);
    ///    Take snapshot of timer and store  
    end_time = get_absolute_time();
    double_time = absolute_time_diff_us(float_end_time, end_time);
    sequential_time = absolute_time_diff_us(start_time, end_time);
    //    Display time taken for application to run in sequential mode
    printf("The single-precision function execution time when using a single-core: %.0fus\n", (float)float_time);
    printf("The double-precision function execution time when using a single-core: %.0fus\n", (float)double_time);
    printf("Total execution time for running the single-precision and double-precision functions in sequentially using a single CPU core: %.0f us\n", (float)sequential_time);

    // Code for parallel run goes here…
    //    Take snapshot of timer and store
    start_time = get_absolute_time();
    //    Run the single-precision Wallis approximation on one core
    multicore_fifo_push_blocking((uintptr_t) &wallis_float);
    multicore_fifo_push_blocking(ITER_MAX);
    //    Run the double-precision Wallis approximation on the other core
    double_start_time = get_absolute_time();
    wallis_double(ITER_MAX);
    int x = multicore_fifo_pop_blocking();
    //get times
    end_time = get_absolute_time();
    float_time = absolute_time_diff_us(start_time, end_time);
    double_time = absolute_time_diff_us(double_start_time, end_time);
    // get total runtime
    parallel_time = absolute_time_diff_us(start_time, end_time);    
    //    Display time taken for application to run in parallel mode
    printf("The single-precision function execution time when using both cores: %.0fus\n", (float)float_time);
    printf("The double-precision function execution time when using both cores: %.0fus\n", (float)double_time);
    printf("Total execution time for running the single-precision and double-precision functions in parallel across both CPU cores: %.0fus\n", (float)parallel_time);

    return 0;
}