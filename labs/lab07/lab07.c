#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.
#include "pico/time.h"      // required for using timestamps

#define PI        3.14159265359 ///< basis for comparison

/**
 * function that meausres the runtimes of another function
 * 
 * 
 * @param precision pointer to a funciton
 * @return elapsed runtime
 */
uint64_t measure_runtime(int (*function)(int), int param) {
  // start the timer
  absolute_time_t start_time = get_absolute_time();
  // call the function to be measured 
  (*function)(param);
  // stop the timer and calculate the elapsed time
  absolute_time_t end_time = get_absolute_time();
  uint64_t elapsed_time = absolute_time_diff_us(start_time, end_time);
  // return the result of the function being measured
  return elapsed_time;
}


// Function to get the enable status of the XIP cache
bool get_xip_cache_en(){
    int *xip_ctrl_base = (int*)XIP_CTRL_BASE; // pointer to address in memory
    return xip_ctrl_base[0] == 1;
}

// Function to set the enable status of the XIP cache
bool set_xip_cache_en(bool cache_en){
    int *xip_ctrl_base = (int*)XIP_CTRL_BASE; // pointer to memory address
    if(cache_en==true)
        xip_ctrl_base[0] = 1; // set bit to 1
    else    
        xip_ctrl_base[0] = 0; // set bit to 0
    return true;
}


/**
 * function that implements the Wallis product algorithm
 * using single-precision floating-point representation
 * 
 * @param precision max number of iterations
 * @return calculated value for PI.
 */

float wallis_float(int precision){
    float result=1;
    float x=1;
    for(int n=1; n<=100000; n++){
        x=2*(float)n;
        result*=(x/(x-1))*(x/(x+1));
    }
    return result*2;
}

/**
 * function that implements the Wallis product algorithm
 * using double-precision floating-point representation
 * 
 * @param precision max number of iterations
 * @return calculated value for PI.
 */

double wallis_double(int precision){
    double result=1;
    double x=1;
    for(int n=1; n<=100000; n++){
        x=2*(double)n;
        result*=(x/(x-1))*(x/(x+1));
    }
    return result*2;
}

/**
 * function that prints datapoints
 * 
 * @param  max number of iterations
 * @return calculated value for PI.
 */

void print_datapoints(uint64_t elapsed_time, uint64_t float_time, uint64_t double_time){
    printf("The single-precision floating-point function: %dus\n", (int)float_time);
    printf("The double-precision floating-point function: %dus\n", (int)float_time);
    printf("The total execution time of the application: %dus\n", (int)float_time);
}

/**
 * 
 * @brief This function acts as the main entry-point for core #1.
 *        A function pointer is passed in via the FIFO with one
 *        incoming int32_t used as a parameter. The function will
 *        provide an int32_t return value by pushing it back on 
 *        the FIFO, which also indicates that the result is ready.
 */
void core1_entry() {
    while (1) {
        // Function pointer is passed to us via the FIFO
        // We have one incoming int32_t as a parameter, and will provide an
        // int32_t return value by simply pushing it back on the FIFO
        // which also indicates the result is ready.
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

// Main code entry point for core0.
int main() {

    const int    ITER_MAX   = 100000;
    absolute_time_t start_time, end_time, double_start_time;
    uint64_t float_time, double_time, elapsed_time;
    int x;

    stdio_init_all();
    

    // Using a single CPU core with caches enabled
    // Enable cache
    set_xip_cache_en(true);
    // Get start time
    start_time = get_absolute_time();
    // The single-precision floating-point function
    float_time = measure_runtime(&wallis_float, ITER_MAX);
    // The double-precision floating-point function
    double_time = measure_runtime(&wallis_double, ITER_MAX);
    // The total execution time of the application
    // Get end time
    end_time = get_absolute_time();
    elapsed_time = absolute_time_diff_us(start_time, end_time);

    printf("Using a single CPU core with caches enabled: \n");
    print_datapoints(elapsed_time, float_time, double_time);

    // Using a single CPU core with caches disabled
    // disable cache
    set_xip_cache_en(false);
    // Get start time
    start_time = get_absolute_time();
    // The single-precision floating-point function
    float_time = measure_runtime(&wallis_float, ITER_MAX);
    // The double-precision floating-point function
    double_time = measure_runtime(&wallis_double, ITER_MAX);
    // The total execution time of the application
    // Get end time
    end_time = get_absolute_time();
    elapsed_time = absolute_time_diff_us(start_time, end_time);

    printf("Using a single CPU core with caches disabled: \n");
    print_datapoints(elapsed_time, float_time, double_time);

    // Using both CPU cores with caches enabled
    multicore_launch_core1(core1_entry);
    // enable cache 
    set_xip_cache_en(true);
    // get start time
    start_time = get_absolute_time();
    // run single-precision function on one core
    multicore_fifo_push_blocking((uintptr_t) &wallis_float);
    multicore_fifo_push_blocking(ITER_MAX);
    // get double start time
    double_start_time = get_absolute_time();
    // run double-precision function on other core
    wallis_double(ITER_MAX);
    x = multicore_fifo_pop_blocking();
    end_time = get_absolute_time();
    // get runtimes
    float_time = absolute_time_diff_us(start_time, end_time);
    double_time = absolute_time_diff_us(double_start_time, end_time);
    elapsed_time = absolute_time_diff_us(start_time, end_time);
    // display runtimes

    printf("Using both CPU cores with caches enabled: \n");
    print_datapoints(elapsed_time, float_time, double_time);

    // Using both CPU cores with caches disabled
    // disable cache 
    set_xip_cache_en(false);
    // get start time
    start_time = get_absolute_time();
    // run single-precision function on one core
    multicore_fifo_push_blocking((uintptr_t) &wallis_float);
    multicore_fifo_push_blocking(ITER_MAX);
    // get double start time
    double_start_time = get_absolute_time();
    // run double-precision function on other core
    wallis_double(ITER_MAX);
    x = multicore_fifo_pop_blocking();
    end_time = get_absolute_time();
    // get runtimes
    float_time = absolute_time_diff_us(start_time, end_time);
    double_time = absolute_time_diff_us(double_start_time, end_time);
    elapsed_time = absolute_time_diff_us(start_time, end_time);
    // display runtimes

    printf("Using both CPU cores with caches disabled: \n");
    print_datapoints(elapsed_time, float_time, double_time);







    


    return 0;
}

